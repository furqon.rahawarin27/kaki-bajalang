@extends('layouts.app')

@section('title')
    Kaki Bajalang
@endsection

@section('content')

    <!-- HEADER -->
    <header class="text-center">
        <h1>Start your adventure <br> in Kei Island</h1>
        <p class="mt-3">You will see beautiful <br> moment you never see before</p>
        <a href="#popular" class="btn btn-get-started px-4 mt-4">Get Started</a>
    </header>

    <main>
        <!-- STATISTIK -->
        <div class="container">
            <section class="section-stats row justify-content-center" id="stats">
                <div class="col-6 col-lg-2 stats-detail">
                    <h2>5</h2>
                    <p>Years Experience</p>
                </div>
                <div class="col-6 col-lg-2 stats-detail">
                    <h2>20K</h2>
                    <p>Members</p>
                </div>
                <div class="col-6 col-lg-2 stats-detail">
                    <h2>10</h2>
                    <p>Destination</p>
                </div>
                <div class="col-6 col-lg-2 stats-detail">
                    <h2>10</h2>
                    <p>Destination</p>
                </div>
            </section>
        </div>

        <!-- POPULAR -->
        <section class="section-popular" id="popular">
            <div class="container">
                <div class="row">
                    <div class="col text-center section-popular-heading">
                        <h2>Wisata Popular</h2>
                        <p>Someting that you never try <br> before in this world</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-popular-content" id="popularContent">
            <div class="container">
                <div class="section-popular-travel row justify-content-center">
                    @foreach ($items as $item)
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="card-travel text-center d-flex flex-column" style="background-image: url('{{$item->galleries->count() ? Storage::url($item->galleries->first()->image) : ''}}');">
                            <div class="travel-country">{{$item->title}}</div>
                            <div class="travel-location">{{$item->location}}</div>
                            <div class="travel-button mt-auto">
                                <a href="{{route('detail', $item->slug)}}" class="btn btn-travel-details px-4">View Details</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>

        <!-- NETWORK -->
        <section class="section-networks">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2>Our Networks</h2>
                        <p>
                            Companies are trusted us <br> more than just a trip
                        </p>
                    </div>
                    <div class="col-md-8 text-center">
                        <img src="{{url('frontend/images/partner.png')}}" alt="Logo Partner" class="img-partner">
                    </div>
                </div>
            </div>
        </section>

        <!-- TESTIMONIAL -->
        <section class="section-testimonial-heading" id="testimonialHeading">
            <div class="container">
                <div class="row">
                    <div class="col text-center">
                        <h2>They Are Loving Us</h2>
                        <p>Moment were giving them <br> the best experience</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-testimonial-content" id="testimonialContent">
            <div class="container">
                <div class="section-popular-travel row justify-content-center">
                    <div class="com-sm-6 col-md-6 col-lg-4">
                        <div class="card card-testimonial text-center">
                            <div class="testimonial-content">
                                <img src="{{url('frontend/images/avatar-1.png')}}" alt="" class="mb-4 rounded-circle">
                                <h3 class="mb-4">Angga</h3>
                                <p class="testimonial">" Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nostrum qui magnam consectetur dicta eligendi "</p>
                                <hr>
                                <p class="trip-to mt-2">Trip to Ubud</p>
                            </div>
                        </div>
                    </div>
                    <div class="com-sm-6 col-md-6 col-lg-4">
                        <div class="card card-testimonial text-center">
                            <div class="testimonial-content">
                                <img src="{{url('frontend/images/avatar-2.png')}}" alt="" class="mb-4 rounded-circle">
                                <h3 class="mb-4">Shayna</h3>
                                <p class="testimonial">" Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nostrum qui magnam consectetur dicta eligendi "</p>
                                <hr>
                                <p class="trip-to mt-2">Trip to Nusa Penida</p>
                            </div>
                        </div>
                    </div>
                    <div class="com-sm-6 col-md-6 col-lg-4">
                        <div class="card card-testimonial text-center">
                            <div class="testimonial-content">
                                <img src="{{url('frontend/images/avatar-3.png')}}" alt="" class="mb-4 rounded-circle">
                                <h3 class="mb-4">Shabrina</h3>
                                <p class="testimonial">" Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nostrum qui magnam consectetur dicta eligendi "</p>
                                <hr>
                                <p class="trip-to mt-2">Trip to Karimun Jawa</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <a href="" class="btn btn-need-help px-4 mt-4 mx-1">I Need Help</a>
                        <a href="{{route('register')}}" class="btn btn-get-started px-4 mt-4 mx-1">Get Started</a>
                    </div>
                </div>
            </div>
        </section>

    </main>
    
@endsection